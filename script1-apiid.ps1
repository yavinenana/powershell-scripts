Write-Host "Congratulations! Your first script executed successfully"
# Comentario hidden

<# Powershell scripts example-hidden
autor: Jordy Peña
team: agile07
HOW TO RUN
/home/jordy/set-apimBackend.ps1 <RESOURCE GROUP> <API MANAGEMENT> <API_ID> #>

$r_group=$args[0]
Write-Host "RESOURCE GROUP NAME: $r_group"

$apim_management=$args[1]
Write-Host "APIM MANAGEMENT NAME: $apim_management"

$api_id=$args[2]
Write-Host "API ID - NAME API (is not the title): $api_id"

#Get-AzApiManagement | select name
$apimContext = New-AzApiManagementContext -ResourceGroupName $r_group -ServiceName $apim_management
Write-Host "$apimContext = New-AzApiManagementContext -ResourceGroupName $r_group -ServiceName $apim_management"

#Get-AzApiManagementApi -Context $apimContext | select name
$var1=Get-AzApiManagementApi -Context $apimContext -ApiId $api_id
$var1
Write-Host "========== api: $($api_name) - in api management: $($apim_management)"
$var2=Get-AzApiManagementApi -Context $apimContext -ApiId $api_id  | select ApiId,Name,Description,ServiceUrl,Path
$var2
#$var2 = Get-AzApiManagementApi -Context $apimContext -Name $api_name # contains properties: ApiId,Name,Description,ServiceUrl,Path
#$var2 | Format-Table -Property ApiId,Name,Description,ServiceUrl,Path
Write-Host "========== operations of api: $($api_name) "
$var3=Get-AzApiManagementOperation -Context $apimContext -ApiId $api_id | select ApiId,Name,Method,UrlTemplate
$var3