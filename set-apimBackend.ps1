Write-Host "SET NEW APIM MANAGEMENT BACKEND - SET URL "
# Comentario hidden

<# Powershell scripts example-hidden
autor: Jordy Peña
team: agile07
HOW TO RUN:
/home/jordy/set-apimBackend.ps1 <RESOURCE GROUP> <API MANAGEMENT> <ENDPOINT> #>


$r_group=$args[0]
echo "RESOURCE GROUP NAME: $r_group"

$apim_management=$args[1]
echo "APIM MANAGEMENT NAME: $apim_management"

$url_endpoint=$args[2]
echo "URL ENDPOINT: $url_endpoint"

# Set AzApiManagement Context
$apimContext = New-AzApiManagementContext -ResourceGroupName $r_group -ServiceName $apim_management

Write-Host "============== GET APIM MANAGEMENT BACKEND REGISTRED BEFORE ===================="
#echo "Get-AzApiManagementBackend -Context $apimContext | select ServiceName,Url | grep $url_endpoint"
Get-AzApiManagementBackend -Context $apimContext | select ServiceName,Url | grep $url_endpoint
Write-Host "================================================================================"


# set ApiManagement
echo "New-AzApiManagementBackend -Context $apimContext -Url $url_endpoint -Protocol http -SkipCertificateChainValidation $true"
New-AzApiManagementBackend -Context $apimContext -Url $url_endpoint -Protocol http -SkipCertificateChainValidation $true

Get-AzApiManagementBackend -Context $apimContext | select ServiceName,Url | grep $url_endpoint